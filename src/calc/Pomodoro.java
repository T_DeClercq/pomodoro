package calc;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;

public class Pomodoro {
	public static void main(String[] args) {
		final Frame pomodoro = new Frame();
		State focus_one = new State("Studeren", 25, Color.red);
		State shortBreak_one = new State("Short break", 10, Color.cyan);
		State focus_two = new State("Studeren", 25, Color.red);
		State shortBreak_two = new State("Short break", 10, Color.cyan);
		State focus_three = new State("Studeren", 25, Color.red);
		State longBreak = new State("Long break", 25, Color.green);
		final FullCycle fullCycle = new FullCycle(pomodoro, focus_one,shortBreak_one, focus_two, shortBreak_two, focus_three, longBreak);
		
		ActionListener actionListener = new ActionListener(){ 
		    @Override
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("HIER BEN IK");

		    	System.out.println(fullCycle.peek().getTime());
		    	if (fullCycle.peek().getTime()<=0 && !fullCycle.isEmpty()){
		    		nextWidget();
		    		System.out.println(fullCycle.peek().getName());
		    	}
		    	else if (fullCycle.isEmpty()){
		    		System.err.println("The cycle is empty");
		    	}
		    	else{
		    		fullCycle.peek().countdown();
		    	}
				
			}
		    private void nextWidget(){
		    	if (fullCycle.peek()!=null){
		    		pomodoro.remove(fullCycle.peek());
		    	}
		    	System.out.println("next widget");
		    	fullCycle.removeFirst();
		    	System.out.println(fullCycle.size());
		    	fullCycle.peek().setVisible(true);
		    }
		}; 
		Timer timer = new Timer(1000,actionListener);
		timer.start();
		}
}
