package calc;

import java.awt.Color;

public class State {
	String state = "ERROR";
	int minutes = 0;
	Color color = Color.RED;
	
	public State(){
		
	}
	
	public State(String state, int minutes, Color color){
		this.state = state;
		if (minutes>0)
			this.minutes = minutes;
		this.color = color;
	}
	
	public String toString(){
		return state;
		
	}

	public int getMinutes() {
		return minutes;
	}
}
