package calc;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.geom.RoundRectangle2D;

import javax.swing.JFrame;

public class Frame extends JFrame {
	private static final long serialVersionUID = 1L;
	private int offset = 10;
	private Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	private double widthDouble = screenSize.getWidth()/4;
	private double heightDouble = screenSize.getHeight()/4;
	private int width = (int)widthDouble;
	private int height = (int)heightDouble;
	private final int rounding = 75;
	public Frame(){
		//SHAPE
		RoundRectangle2D shape = new RoundRectangle2D.Double(0, 0, width, height, rounding, rounding);
				
		//FRAME
		
		this.setVisible(false);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setAlwaysOnTop(true);
		this.setUndecorated(true);
		this.setBounds(offset, offset, width+offset, height+offset);
		this.setShape(shape);
		this.setOpacity(0.55f);
		this.setTitle(this.getName().toUpperCase());
	}
	public int getOffset() {
		return offset;
	}


}
