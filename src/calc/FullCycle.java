package calc;

import java.util.LinkedList;

public class FullCycle extends LinkedList<Widget> {

	private static final long serialVersionUID = 1L;

	public FullCycle(Frame frame, State...states){
		for(State state:states){
			this.add(new Widget(frame, state));
		}
	}
}
