package calc;

import java.awt.Color;
import java.awt.Font;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
/**
 * Widget where time and state of the Pomodoro app will be shown. The only visible window of this application.
 * @author Thomas De Clercq
 * @version 0.1b
 *
 */
public class Widget extends JPanel {
	private static final long serialVersionUID = 1L;

	private JLabel text = new JLabel();
	private JLabel time = new JLabel();
	private State state = new State();
	private int seconds = 0;
	
	public Widget(Frame frame, State state){
		
		//STATE
		this.setState(state);
		
		//GETTERS
		int offset = frame.getOffset();
		int width = frame.getWidth();
		int height = frame.getHeight();
		

		//BORDER
		Border border = BorderFactory.createLineBorder(Color.black);
		
		//PANEL
		this.setBorder(border);
		this.setBounds(offset, offset, width+offset, height+offset);
		this.setVisible(true);
		frame.add(this);
		
		//TEXT
		text = new JLabel(state.toString(),SwingConstants.CENTER);
		text.setFont(new Font("Tahoma", Font.PLAIN, this.getHeight()/4));
		text.setForeground(Color.WHITE);
		this.add(text);
				
		//TIME
		time = new JLabel();
		setTime(minsToSecs(state.getMinutes()));
	    time.setFont(new Font("Tahoma", Font.PLAIN, this.getHeight()/3));
		time.setForeground(Color.WHITE);
	    this.add(time);
	    
	    //FRAME
		
	    
	}
	public int getTimeMins(){
		return state.getMinutes();
	}
	public String getText(){
		return text.getText();
	}
	private void setTime(int secs){
		seconds = secs;
		time.setText(secsToTime(secs)[0]+":"+secsToTime(secs)[1]);
	}
	private int[] secsToTime(int secs){
		int timeSecs = secs%60;
		int timeMins = secs/60;
		int[] time = {timeSecs,timeMins};
		return time;
	}
	private int minsToSecs(int mins){
		return mins*60;
	}
	void countdown(){
		setTime(getTime()-1);
	}
	int getTime() {
		return seconds;
	}
	/*
	public void fadeOut(){
		float alpha = frame.getOpacity();
		for (float i = 10; i>0; i--){
			frame.setOpacity(alpha*(i/10));
		}
		this.setVisible(false);
	}
	public void fadeIn(){
		float alpha = 0.55f;
		this.setOpacity(0f);
		this.setVisible(true);
		for (int i = 0; i<10; i++){
			float opacity = (alpha*(i/10));
			this.setOpacity(opacity);
		}
	}*/
	private void setState(State state){
		this.state = state;
	}
	public Widget(){
		this.setTime(-1);
	}
}
